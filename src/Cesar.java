//Sergio Moreno Albert - Entornos
public class Cesar {
	public String mensaje;
	public String mensajeCodificado;
	public String tmp;
	public int saltos;
	
	public Cesar(String mensaje, int saltos){
		this.mensaje=mensaje;
		this.saltos=saltos;
	}
	/*
	 * Encriptamos el mensaje a partir unos saltos recibidos desde main
	 */
	public void encriptaMensaje(){
		tmp=mensaje;
		for(int i = 0;i<=saltos;i++){
			tmp=tmp.substring(1, tmp.length())+tmp.substring(0,1);
		}
		this.mensajeCodificado=tmp;
	}
	/*
	 * Mostramos el mensaje codificado
	 */
	public void muestraCodificado(){
		System.out.println("Mensaje codificado: "+this.mensajeCodificado);
	}
}